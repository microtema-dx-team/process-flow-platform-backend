## Process-Flow Platform Backend

# create docker image
`docker build -t microtema/process-flow-platform-backend:1.0.0 .`

# run docker file
`docker run --name process-flow-platform-backend -p 8081:8080 microtema/process-flow-platform-backend:1.0.0`

# push docker file
`docker push microtema/process-flow-platform-backend:1.0.0`

# db migration
`mvn flyway:migrate -P db-migration`

# REST Endpoints

## Definitions
* @GET /rest/definition?page=0&size=10&query=foo&properties=!definitionName
* @GET /rest/definition/{id}

## Reports
* @GET /rest/report
* @GET /rest/report/{id}

## Processes
* @GET /rest/process
* @GET /rest/process/{id}