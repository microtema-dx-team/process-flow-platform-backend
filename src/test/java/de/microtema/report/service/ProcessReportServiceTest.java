package de.microtema.report.service;

import de.microtema.process.service.ProcessInstanceWriteService;
import de.microtema.report.converter.ProcessReportEventToProcessReportEntityConverter;
import de.microtema.report.repository.ProcessReportEntity;
import de.microtema.report.repository.ProcessReportRepository;
import de.microtema.report.update.ProcessReportEntityUpdater;
import de.microtema.report.vo.ProcessReportEvent;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ProcessReportServiceTest {

    @InjectMocks
    ProcessReportService sut;

    @Mock
    ProcessReportRepository repository;

    @Mock
    ProcessReportEntityUpdater entityUpdater;

    @Mock
    ProcessReportEventToProcessReportEntityConverter entityConverter;

    @Mock
    ProcessInstanceWriteService processInstanceWriteService;

    @Mock
    ProcessReportEvent processReportEvent;

    @Mock
    ProcessReportEntity processReportEntity;

    @Mock
    ProcessReportEntity recent;

    @Test
    void saveProcessReport() {

        when(entityConverter.convert(processReportEvent)).thenReturn(processReportEntity);
        when(repository.findOne(processReportEntity)).thenReturn(Optional.empty());

        sut.saveProcessReport(processReportEvent);

        verify(entityConverter).convert(processReportEvent);
        verify(repository).findOne(processReportEntity);
        verify(repository).saveAndFlush(processReportEntity);
        verify(entityUpdater, never()).update(any(), any());
        verify(processInstanceWriteService).updateProcessInstance(processReportEvent);
    }

    @Test
    void updateProcessReport() {

        when(entityConverter.convert(processReportEvent)).thenReturn(processReportEntity);
        when(repository.findOne(processReportEntity)).thenReturn(Optional.of(recent));

        sut.saveProcessReport(processReportEvent);

        verify(entityConverter).convert(processReportEvent);
        verify(repository).findOne(processReportEntity);
        verify(repository).saveAndFlush(recent);
        verify(entityUpdater).update(recent, processReportEntity);
        verify(processInstanceWriteService).updateProcessInstance(processReportEvent);
    }
}
