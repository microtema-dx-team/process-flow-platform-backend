package de.microtema.report.converter;

import de.microtema.common.enums.ProcessStatus;
import de.microtema.model.builder.annotation.Model;
import de.microtema.model.builder.enums.ModelType;
import de.microtema.model.builder.util.FieldInjectionUtil;
import de.microtema.report.repository.ProcessReportEntity;
import de.microtema.report.vo.ProcessReportEvent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import static org.junit.jupiter.api.Assertions.*;

class ProcessReportEventToProcessReportEntityConverterTest {

    @Inject
    ProcessReportEventToProcessReportEntityConverter sut;

    @Model(type = ModelType.MAX)
    ProcessReportEvent processReportEvent;

    @BeforeEach
    void setUp() {

        FieldInjectionUtil.injectFields(this);
    }

    @Test
    void convert() {

        ProcessReportEntity answer = sut.convert(processReportEvent);

        assertNotNull(answer);
        assertEquals(processReportEvent.getProcessBusinessKey(), answer.getProcessBusinessKey());
        assertEquals(processReportEvent.getProcessStatus(), answer.getReportStatus());
        assertEquals(processReportEvent.getActivityId(), answer.getReportId());
        assertEquals(processReportEvent.getActivityName(), answer.getReportName());
        assertEquals(processReportEvent.getDescription(), answer.getReportDescription());
    }

    @Test
    void convertOnSTartEvent() {

        processReportEvent.setProcessStatus(ProcessStatus.STARTED);

        ProcessReportEntity answer = sut.convert(processReportEvent);

        assertEquals(processReportEvent.getProcessStatus(), answer.getReportStatus());
        assertEquals(processReportEvent.getEventTime(), answer.getReportStartTime());
        assertNull(answer.getReportEndTime());
    }

    @Test
    void convertOnEndEvent() {

        processReportEvent.setProcessStatus(ProcessStatus.ENDED);

        ProcessReportEntity answer = sut.convert(processReportEvent);

        assertEquals(processReportEvent.getProcessStatus(), answer.getReportStatus());
        assertEquals(processReportEvent.getEventTime(), answer.getReportEndTime());
        assertNull(answer.getReportStartTime());
    }

    @Test
    void convertOnCompleteEvent() {

        processReportEvent.setProcessStatus(ProcessStatus.COMPLETED);

        ProcessReportEntity answer = sut.convert(processReportEvent);

        assertEquals(processReportEvent.getProcessStatus(), answer.getReportStatus());
        assertEquals(processReportEvent.getEventTime(), answer.getReportEndTime());
        assertNull(answer.getReportStartTime());
    }
}