package de.microtema.report.update;

import de.microtema.common.enums.ProcessStatus;
import de.microtema.model.builder.annotation.Model;
import de.microtema.model.builder.enums.ModelType;
import de.microtema.model.builder.util.FieldInjectionUtil;
import de.microtema.report.repository.ProcessReportEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class ProcessReportEntityUpdaterTest {

    @Inject
    ProcessReportEntityUpdater sut;

    @Model(type = ModelType.MAX)
    ProcessReportEntity processReportEntity;

    @BeforeEach
    void setUp() {

        FieldInjectionUtil.injectFields(this);
    }

    @Test
    void update() {

        ProcessReportEntity recent = new ProcessReportEntity();

        sut.update(recent, processReportEntity);

        assertEquals(recent.getReportStatus(), processReportEntity.getReportStatus());
        assertEquals(recent.getReportEndTime(), processReportEntity.getReportEndTime());
    }

    @Test
    void updateOnGreaterThenStatus() {

        ProcessReportEntity recent = new ProcessReportEntity();

        recent.setReportStatus(ProcessStatus.STARTED);

        processReportEntity.setReportStatus(ProcessStatus.ENDED);

        sut.update(recent, processReportEntity);

        assertEquals(recent.getReportStatus(), processReportEntity.getReportStatus());
        assertEquals(recent.getReportEndTime(), processReportEntity.getReportEndTime());
    }

    @Test
    void updateOnLesThenStatus() {

        ProcessReportEntity recent = new ProcessReportEntity();

        recent.setReportStatus(ProcessStatus.ENDED);

        processReportEntity.setReportStatus(ProcessStatus.STARTED);

        sut.update(recent, processReportEntity);

        assertEquals(recent.getReportStatus(), ProcessStatus.ENDED);
        assertNull(recent.getReportEndTime());
    }
}