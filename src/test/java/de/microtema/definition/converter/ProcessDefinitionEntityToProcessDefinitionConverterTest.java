package de.microtema.definition.converter;

import de.microtema.definition.repository.ProcessDefinitionEntity;
import de.microtema.definition.vo.ProcessDefinition;
import de.microtema.model.builder.annotation.Model;
import de.microtema.model.builder.enums.ModelType;
import de.microtema.model.builder.util.FieldInjectionUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class ProcessDefinitionEntityToProcessDefinitionConverterTest {

    @Inject
    ProcessDefinitionEntityToProcessDefinitionConverter sut;

    @Model(type = ModelType.MAX)
    ProcessDefinitionEntity model;

    @BeforeEach
    void setUp() {
        FieldInjectionUtil.injectFields(this);
    }

    @Test
    void convert() {

        ProcessDefinition answer = sut.convert(model);

        assertNotNull(answer);

        assertEquals(model.getDefinitionKey(), answer.getDefinitionKey());
        assertEquals(model.getBoundedContext(), answer.getBoundedContext());
        assertEquals(model.getDefinitionName(), answer.getName());
        assertEquals(model.getDefinitionDisplayName(), answer.getDisplayName());
        assertEquals(model.getDefinitionDescription(), answer.getDescription());

        assertEquals(model.getDefinitionDeployTime(), answer.getDeployTime());
        assertEquals(model.getDefinitionMajorVersion(), answer.getMajorVersion());
        assertEquals(model.getDefinitionVersion(), answer.getDefinitionVersion());

        assertEquals(model.getDefinitionDiagram(), answer.getDiagram());
        assertEquals(model.getUuid(), answer.getId());
    }
}
