package de.microtema.process.converter;

import de.microtema.common.enums.ProcessStatus;
import de.microtema.model.builder.annotation.Model;
import de.microtema.model.builder.enums.ModelType;
import de.microtema.model.builder.util.FieldInjectionUtil;
import de.microtema.process.repository.ProcessInstanceEntity;
import de.microtema.process.vo.ProcessStartedEvent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import static org.junit.jupiter.api.Assertions.*;

class ProcessStartEventToProcessInstanceEntityConverterTest {

    @Inject
    ProcessStartEventToProcessInstanceEntityConverter sut;

    @Model(type = ModelType.MAX)
    ProcessStartedEvent model;

    @BeforeEach
    void setUp() {
        FieldInjectionUtil.injectFields(this);
    }

    @Test
    void convert() {

        ProcessInstanceEntity answer = sut.convert(model);

        assertNotNull(answer);
        assertEquals(model.getDefinitionKey(), answer.getDefinitionKey());
        assertNull(answer.getInstanceEndTime());
        assertNull(answer.getInstanceId());
        assertEquals(ProcessStatus.QUEUED, answer.getInstanceStatus());
        assertEquals(ProcessStatus.QUEUED.getDisplayName(), answer.getInstanceDescription());

        assertEquals(model.getStartTime(), answer.getInstanceStartTime());
        assertEquals(model.getBusinessKey(), answer.getProcessBusinessKey());
    }
}