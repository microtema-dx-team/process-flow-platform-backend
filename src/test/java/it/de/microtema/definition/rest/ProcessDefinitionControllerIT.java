package it.de.microtema.definition.rest;

import de.microtema.BackendApplication;
import de.microtema.definition.rest.ProcessDefinitionController;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.crypto.SecretKey;
import javax.inject.Inject;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;

@EnableKafka
@EmbeddedKafka(partitions = 1, brokerProperties = {"listeners=PLAINTEXT://localhost:3333", "port=3333"})
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {BackendApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ProcessDefinitionControllerIT {

    @Inject
    ProcessDefinitionController sut;

    @Inject
    TestRestTemplate restTemplate;

    String url = "/rest/definition";

    @BeforeEach
    public void setup() {

        restTemplate.getRestTemplate().setInterceptors(Collections.singletonList(((request, body, execution) -> {

            String clientId = "0oa3h0mq2kwRxe9hO5d6";
            String clientSecret = "00mw6etKkg5WP4d8kTQIE7RzVb9AvuK938iiB3u8ND";
            SecretKey secretKey = Keys.hmacShaKeyFor(clientSecret.getBytes(StandardCharsets.UTF_8));
            Instant now = Instant.now();

            String token = Jwts.builder()
                    .setAudience("https://dev-1065782.okta.com/oauth2/default/v1/token")
                    .setIssuedAt(Date.from(now))
                    .setExpiration(Date.from(now.plus(5L, ChronoUnit.MINUTES)))
                    .setIssuer(clientId)
                    .setSubject(clientId)
                    .setId(UUID.randomUUID().toString())
                    .signWith(secretKey)
                    .compact();

            HttpHeaders headers = request.getHeaders();

            headers.add("Authorization", token);

            return execution.execute(request, body);
        })));
    }

    @Test
    void processDefinitions() {

        url += "?page=0&size=10&query=foo&properties=!definitionName";

        ResponseEntity<Map> response = restTemplate.exchange(url, HttpMethod.GET, null, Map.class);

        assertNotNull(response);
        assertSame(HttpStatus.UNAUTHORIZED, response.getStatusCode());
        assertNotNull(response.getBody());
    }
}
