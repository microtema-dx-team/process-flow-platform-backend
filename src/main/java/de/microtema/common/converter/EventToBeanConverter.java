package de.microtema.common.converter;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.microtema.model.converter.MetaConverter;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class EventToBeanConverter<T> implements MetaConverter<T, String, Class<T>> {

    private final ObjectMapper objectMapper;

    @SneakyThrows
    public T convert(String orig, Class<T> type) {

        return objectMapper.readValue(orig, type);
    }
}
