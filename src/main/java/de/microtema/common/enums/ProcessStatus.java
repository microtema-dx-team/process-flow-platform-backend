package de.microtema.common.enums;

import java.util.Objects;

public enum ProcessStatus {

    QUEUED("Queued"),
    STARTED("Started"),
    PROCESSING("Processing"),
    SUSPENDED("Suspended"),
    ENDED("Ended"),
    COMPLETED("Completed"),
    WARNING("Warning"),
    ABORTED("Aborted"),
    ERROR("Error");

    private final String displayName;

    ProcessStatus(String displayName) {
        this.displayName = displayName;
    }

    public boolean before(ProcessStatus b) {

        if (Objects.isNull(b)) {
            return false;
        }

        return ordinal() < b.ordinal();
    }

    public boolean after(ProcessStatus b) {

        if (Objects.isNull(b)) {
            return false;
        }

        return ordinal() > b.ordinal();
    }

    public String getDisplayName() {

        return displayName;
    }
}
