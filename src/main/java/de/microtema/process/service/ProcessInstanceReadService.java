package de.microtema.process.service;

import de.microtema.common.converter.PageRequestDTOToPageRequestConverter;
import de.microtema.definition.vo.PageRequestDTO;
import de.microtema.process.converter.ProcessInstanceEntityToProcessInstanceConverter;
import de.microtema.process.converter.ProcessInstanceSpecificationToSpecificationConverter;
import de.microtema.process.repository.ProcessInstanceEntity;
import de.microtema.process.repository.ProcessInstanceRepository;
import de.microtema.process.vo.ProcessInstance;
import de.microtema.process.vo.ProcessInstanceSpecification;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class ProcessInstanceReadService {

    private final ProcessInstanceRepository repository;
    private final PageRequestDTOToPageRequestConverter pageRequestConverter;
    private final ProcessInstanceEntityToProcessInstanceConverter processInstanceConverter;
    private final ProcessInstanceSpecificationToSpecificationConverter specificationConverter;

    public Page<ProcessInstance> getProcesses(int page, int size, String properties, String query) {

        PageRequestDTO pageRequestDTO = PageRequestDTO.of(page, size, properties);
        Pageable pageable = pageRequestConverter.convert(pageRequestDTO);
        ProcessInstanceSpecification processInstanceSpecification = ProcessInstanceSpecification.builder().query(query).build();
        Specification<ProcessInstanceEntity> specification = specificationConverter.convert(processInstanceSpecification);

        return repository.findAll(specification, pageable).map(processInstanceConverter::convert);
    }

    public ProcessInstance getProcessByProcessBusinessKey(String processBusinessKey) {

        return repository.findByProcessBusinessKey(processBusinessKey).map(processInstanceConverter::convert).orElse(null);
    }
}
