package de.microtema.process.service;

import de.microtema.process.converter.ProcessReportEventToProcessInstanceEntityConverter;
import de.microtema.process.converter.ProcessStartEventToProcessInstanceEntityConverter;
import de.microtema.process.converter.ProcessStartedEventToProcessInstanceEntityConverter;
import de.microtema.process.repository.ProcessInstanceEntity;
import de.microtema.process.repository.ProcessInstanceRepository;
import de.microtema.process.updater.ProcessInstanceEntityUpdater;
import de.microtema.process.vo.ProcessStartEvent;
import de.microtema.process.vo.ProcessStartedEvent;
import de.microtema.report.vo.ProcessReportEvent;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
@RequiredArgsConstructor
public class ProcessInstanceWriteService {

    private final ProcessInstanceEntityUpdater entityUpdater;
    private final ProcessInstanceRepository processInstanceRepository;
    private final ProcessStartEventToProcessInstanceEntityConverter processStartEventConverter;
    private final ProcessStartedEventToProcessInstanceEntityConverter processStartedEventConverter;
    private final ProcessReportEventToProcessInstanceEntityConverter eventToProcessInstanceEntityConverter;

    public void saveProcessInstance(ProcessStartEvent processStartEvent) {

        ProcessInstanceEntity processInstanceEntity = processStartEventConverter.convert(processStartEvent);

        saveProcessInstance(processInstanceEntity);
    }

    public void updateProcessInstance(ProcessStartedEvent processStartEvent) {

        ProcessInstanceEntity processInstanceEntity = processStartedEventConverter.convert(processStartEvent);

        saveProcessInstance(processInstanceEntity);
    }

    public synchronized void updateProcessInstance(ProcessReportEvent processReportEvent) {

        String processBusinessKey = processReportEvent.getProcessBusinessKey();

        Optional<ProcessInstanceEntity> processInstance = processInstanceRepository.findByProcessBusinessKey(processBusinessKey);

        processInstance.ifPresent(it -> eventToProcessInstanceEntityConverter.update(it, processReportEvent));
    }

    /*
     * NOTE: We need to synchronized the block since we have async kafka events and call flush after save.
     */
    private synchronized void saveProcessInstance(ProcessInstanceEntity processInstanceEntity) {

        Optional<ProcessInstanceEntity> recent = processInstanceRepository.findOne(processInstanceEntity);

        if (recent.isPresent()) {

            updateProcessInstance(recent.get(), processInstanceEntity);

        } else {

            processInstanceRepository.saveAndFlush(processInstanceEntity);
        }
    }

    private void updateProcessInstance(ProcessInstanceEntity recent, ProcessInstanceEntity processInstanceEntity) {

        entityUpdater.update(recent, processInstanceEntity);

        processInstanceRepository.saveAndFlush(recent);
    }
}
