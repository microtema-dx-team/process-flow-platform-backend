package de.microtema.process.kafka;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "started-process")
public class StartedProcessProperties {

    private String topicName;

    private String errorTopicName;
}
