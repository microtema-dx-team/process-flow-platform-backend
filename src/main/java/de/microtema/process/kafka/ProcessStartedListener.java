package de.microtema.process.kafka;

import de.microtema.common.converter.EventToBeanConverter;
import de.microtema.process.service.ProcessInstanceWriteService;
import de.microtema.process.vo.ProcessStartedEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

@Log4j2
@Component
@RequiredArgsConstructor
public class ProcessStartedListener {

    private final ProcessInstanceWriteService service;
    private final StartedProcessProperties startedProcessProperties;
    private final EventToBeanConverter<ProcessStartedEvent> eventConverter;

    @SendTo("#{startedProcessProperties.errorTopicName}")
    @KafkaListener(topics = "#{startedProcessProperties.topicName}", errorHandler = "kafkaListenerErrorHandler")
    public void listen(@Payload String event) {

        log.info(() -> "Receive event: " + startedProcessProperties.getTopicName());

        ProcessStartedEvent processStartedEvent = eventConverter.convert(event, ProcessStartedEvent.class);

        service.updateProcessInstance(processStartedEvent);
    }
}
