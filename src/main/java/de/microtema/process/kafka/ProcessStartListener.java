package de.microtema.process.kafka;

import de.microtema.common.converter.EventToBeanConverter;
import de.microtema.process.service.ProcessInstanceWriteService;
import de.microtema.process.vo.ProcessStartEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

@Log4j2
@Component
@RequiredArgsConstructor
public class ProcessStartListener {

    private final ProcessInstanceWriteService service;
    private final StartProcessProperties startProcessProperties;
    private final EventToBeanConverter<ProcessStartEvent> eventConverter;

    @SendTo("#{startProcessProperties.errorTopicName}")
    @KafkaListener(topics = "#{startProcessProperties.topicName}", errorHandler = "kafkaListenerErrorHandler")
    public void listen(@Payload String event) {

        log.info(() -> "Receive event: " + startProcessProperties.getTopicName());

        ProcessStartEvent processStartedEvent = eventConverter.convert(event, ProcessStartEvent.class);

        service.saveProcessInstance(processStartedEvent);
    }
}
