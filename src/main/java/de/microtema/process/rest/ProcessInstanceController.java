package de.microtema.process.rest;

import de.microtema.process.facade.ProcessInstanceFacade;
import de.microtema.process.vo.ProcessInstance;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/rest/process")
public class ProcessInstanceController {

    private final ProcessInstanceFacade facade;

    @GetMapping
    public ResponseEntity<Page<ProcessInstance>> getProcesses(
            @RequestParam(name = "page", required = false, defaultValue = "0") int page,
            @RequestParam(name = "size", required = false, defaultValue = "25") int size,
            @RequestParam(name = "properties", required = false, defaultValue = "instanceStartTime") String properties,
            @RequestParam(name = "query", required = false) String query) {

        return ResponseEntity.ok(facade.getProcesses(page, size, properties, query));
    }

    @GetMapping(value = "/{processBusinessKey}")
    public ResponseEntity<ProcessInstance> getProcessByProcessBusinessKey(@PathVariable String processBusinessKey) {

        return ResponseEntity.ok(facade.getProcessByProcessBusinessKey(processBusinessKey));
    }
}
