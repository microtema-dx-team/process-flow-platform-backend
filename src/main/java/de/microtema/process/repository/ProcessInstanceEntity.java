package de.microtema.process.repository;

import de.microtema.common.enums.ProcessStatus;
import de.microtema.common.repository.BaseEntity;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@Entity(name = "ProcessInstance")
@Table(name = "ProcessInstance")
public class ProcessInstanceEntity extends BaseEntity implements Comparable<ProcessInstanceEntity> {

    /**
     * definitionKey is set of process-id, version and deployment-id
     */
    @NotNull
    private String definitionKey;

    /**
     * instanceName is needed for query search
     */
    @NotNull
    private String instanceName;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private ProcessStatus instanceStatus;

    @NotNull
    private String instanceDescription;

    /**
     * InstanceId may be null at initial and it will be set from process engine
     */
    private String instanceId;

    /**
     * Process BusinessKey it will be generated from system and passed to process engine
     */
    @NotNull
    private String processBusinessKey;

    @NotEmpty
    private String starterId;

    @NotEmpty
    private String referenceId;

    @NotEmpty
    private String referenceType;

    @NotNull
    private String referenceValue;

    @NotNull
    private LocalDateTime instanceStartTime;

    private LocalDateTime instanceEndTime;

    @Override
    public int compareTo(ProcessInstanceEntity o) {

        return o.getInstanceStartTime().compareTo(instanceStartTime);
    }
}
