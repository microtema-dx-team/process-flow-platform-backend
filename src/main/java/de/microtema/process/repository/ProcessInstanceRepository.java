package de.microtema.process.repository;

import de.microtema.common.repository.BaseEntityRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProcessInstanceRepository extends BaseEntityRepository<ProcessInstanceEntity> {

    @Override
    default Optional<ProcessInstanceEntity> findOne(ProcessInstanceEntity entity) {

        String processBusinessKey = entity.getProcessBusinessKey();

        return findByProcessBusinessKey(processBusinessKey);
    }

    Optional<ProcessInstanceEntity> findByProcessBusinessKey(String instanceBusinessKey);
}
