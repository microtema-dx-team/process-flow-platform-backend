package de.microtema.process.updater;

import de.microtema.common.enums.ProcessStatus;
import de.microtema.model.converter.Updater;
import de.microtema.process.repository.ProcessInstanceEntity;
import org.springframework.stereotype.Component;

@Component
public class ProcessInstanceEntityUpdater implements Updater<ProcessInstanceEntity, ProcessInstanceEntity> {

    @Override
    public void update(ProcessInstanceEntity dest, ProcessInstanceEntity orig) {

        ProcessStatus instanceStatus = dest.getInstanceStatus();

        boolean validStatus = orig.getInstanceStatus().before(instanceStatus);

        if (validStatus) {
            return;
        }

        dest.setInstanceStatus(orig.getInstanceStatus());
        dest.setInstanceId(orig.getInstanceId());
        dest.setInstanceEndTime(orig.getInstanceEndTime());
        dest.setInstanceDescription(orig.getInstanceDescription());
    }
}
