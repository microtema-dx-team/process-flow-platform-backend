package de.microtema.process.converter;

import de.microtema.common.enums.ProcessStatus;
import de.microtema.model.converter.Converter;
import de.microtema.process.repository.ProcessInstanceEntity;
import de.microtema.process.vo.ProcessStartedEvent;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ProcessStartedEventToProcessInstanceEntityConverter implements Converter<ProcessInstanceEntity, ProcessStartedEvent> {

    private final ProcessStartEventToProcessInstanceEntityConverter processStartEventToProcessInstanceEntityConverter;

    @Override
    public void update(ProcessInstanceEntity dest, ProcessStartedEvent orig) {

        processStartEventToProcessInstanceEntityConverter.update(dest, orig);

        dest.setInstanceStatus(ProcessStatus.STARTED);
        dest.setInstanceDescription(dest.getInstanceStatus().getDisplayName());

        dest.setInstanceId(orig.getProcessInstanceId());
    }
}
