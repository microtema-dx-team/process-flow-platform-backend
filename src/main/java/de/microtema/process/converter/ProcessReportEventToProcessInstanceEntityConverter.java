package de.microtema.process.converter;

import de.microtema.common.enums.ProcessStatus;
import de.microtema.model.converter.Converter;
import de.microtema.process.repository.ProcessInstanceEntity;
import de.microtema.report.vo.ProcessReportEvent;
import org.springframework.stereotype.Component;

@Component
public class ProcessReportEventToProcessInstanceEntityConverter implements Converter<ProcessInstanceEntity, ProcessReportEvent> {

    @Override
    public void update(ProcessInstanceEntity dest, ProcessReportEvent orig) {

        ProcessStatus processStatus = orig.getProcessStatus();

        dest.setInstanceStatus(processStatus);
        dest.setInstanceDescription(orig.getDescription());

        switch (processStatus) {
            case SUSPENDED:
            case ABORTED:
            case WARNING:
            case ERROR:
            case COMPLETED:
                dest.setInstanceEndTime(orig.getEventTime());
                break;
        }
    }
}
