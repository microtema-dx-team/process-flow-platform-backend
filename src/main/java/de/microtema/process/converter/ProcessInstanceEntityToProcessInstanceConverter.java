package de.microtema.process.converter;

import de.microtema.model.converter.Converter;
import de.microtema.process.repository.ProcessInstanceEntity;
import de.microtema.process.vo.ProcessInstance;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

@Component
public class ProcessInstanceEntityToProcessInstanceConverter implements Converter<ProcessInstance, ProcessInstanceEntity> {

    @Override
    public void update(ProcessInstance dest, ProcessInstanceEntity orig) {

        dest.setInstanceId(orig.getInstanceId());
        dest.setProcessBusinessKey(orig.getProcessBusinessKey());
        dest.setInstanceStatus(orig.getInstanceStatus());
        dest.setInstanceDescription(orig.getInstanceDescription());
        dest.setInstanceStatus(orig.getInstanceStatus());

        dest.setStarterId(orig.getStarterId());

        dest.setReferenceType(orig.getReferenceType());
        dest.setReferenceId(orig.getReferenceId());
        dest.setReferenceValue(orig.getReferenceValue());

        dest.setInstanceStartTime(orig.getInstanceStartTime());
        dest.setInstanceEndTime(orig.getInstanceEndTime());
        dest.setDuration(getDuration(dest.getInstanceStartTime(), dest.getInstanceEndTime()));

        dest.setDefinitionKey(orig.getDefinitionKey());
        dest.setInstanceName(orig.getInstanceName());
    }

    private long getDuration(LocalDateTime instanceStartTime, LocalDateTime instanceEndTime) {

        return ChronoUnit.MILLIS.between(instanceStartTime, Optional.ofNullable(instanceEndTime).orElse(LocalDateTime.now()));
    }
}
