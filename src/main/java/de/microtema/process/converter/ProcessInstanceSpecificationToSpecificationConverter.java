package de.microtema.process.converter;

import de.microtema.model.converter.Converter;
import de.microtema.process.repository.ProcessInstanceEntity;
import de.microtema.process.vo.ProcessInstanceSpecification;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Component
public class ProcessInstanceSpecificationToSpecificationConverter implements Converter<Specification<ProcessInstanceEntity>, ProcessInstanceSpecification> {

    @Override
    public Specification<ProcessInstanceEntity> convert(ProcessInstanceSpecification orig) {
        return (root, criteriaQuery, criteriaBuilder) -> {

            Predicate predicate = getOrPredicate(orig, root, criteriaBuilder);

            return criteriaBuilder.and(predicate);
        };
    }

    private Predicate getOrPredicate(ProcessInstanceSpecification orig, Root<ProcessInstanceEntity> root, CriteriaBuilder criteriaBuilder) {

        List<Predicate> predicates = new ArrayList<>();

        Predicate predicate = getLikePredicate(root, criteriaBuilder, "instanceName", StringUtils.trimToEmpty(orig.getQuery()));
        CollectionUtils.addIgnoreNull(predicates, predicate);

        predicate = getLikePredicate(root, criteriaBuilder, "instanceDescription", orig.getQuery());
        CollectionUtils.addIgnoreNull(predicates, predicate);

        predicate = getLikePredicate(root, criteriaBuilder, "instanceId", orig.getQuery());
        CollectionUtils.addIgnoreNull(predicates, predicate);

        predicate = getLikePredicate(root, criteriaBuilder, "instanceBusinessKey", orig.getQuery());
        CollectionUtils.addIgnoreNull(predicates, predicate);

        predicate = getLikePredicate(root, criteriaBuilder, "starterId", orig.getQuery());
        CollectionUtils.addIgnoreNull(predicates, predicate);

        predicate = getLikePredicate(root, criteriaBuilder, "referenceId", orig.getQuery());
        CollectionUtils.addIgnoreNull(predicates, predicate);

        predicate = getLikePredicate(root, criteriaBuilder, "referenceType", orig.getQuery());
        CollectionUtils.addIgnoreNull(predicates, predicate);

        predicate = getLikePredicate(root, criteriaBuilder, "referenceValue", orig.getQuery());
        CollectionUtils.addIgnoreNull(predicates, predicate);

        if (CollectionUtils.isEmpty(predicates)) {
            return null;
        }

        return criteriaBuilder.or(predicates.toArray(new Predicate[0]));
    }

    private Predicate getLikePredicate(Root<ProcessInstanceEntity> root, CriteriaBuilder criteriaBuilder, String property, String query) {

        if (Objects.isNull(query)) {
            return null;
        }

        Path<String> path = root.get(property);

        return criteriaBuilder.like(criteriaBuilder.lower(path), "%" + query.toLowerCase() + "%");
    }
}