package de.microtema.process.converter;

import de.microtema.common.enums.ProcessStatus;
import de.microtema.model.converter.Converter;
import de.microtema.process.repository.ProcessInstanceEntity;
import de.microtema.process.vo.ProcessStartEvent;
import org.springframework.stereotype.Component;

@Component
public class ProcessStartEventToProcessInstanceEntityConverter implements Converter<ProcessInstanceEntity, ProcessStartEvent> {

    @Override
    public void update(ProcessInstanceEntity dest, ProcessStartEvent orig) {

        dest.setInstanceStartTime(orig.getStartTime());

        dest.setStarterId(orig.getStarterId());
        dest.setReferenceId(orig.getReferenceId());
        dest.setReferenceType(orig.getReferenceType());
        dest.setReferenceValue(orig.getReferenceValue());

        dest.setDefinitionKey(orig.getDefinitionKey());
        dest.setProcessBusinessKey(orig.getBusinessKey());

        dest.setInstanceName(orig.getProcessName());

        dest.setInstanceStatus(ProcessStatus.QUEUED);
        dest.setInstanceDescription(dest.getInstanceStatus().getDisplayName());
    }
}
