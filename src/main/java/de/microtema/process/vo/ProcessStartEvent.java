package de.microtema.process.vo;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
public class ProcessStartEvent {

    /**
     * definitionKey is set of process-id, version and deployment-id
     */
    @NotNull
    private String definitionKey;

    @NotNull
    private String processName;

    @NotNull
    private String boundedContext;

    @NotEmpty
    private String starterId;

    @NotEmpty
    private String referenceId;

    @NotEmpty
    private String referenceType;

    @NotNull
    private String referenceValue;

    @NotNull
    private String businessKey;

    @NotNull
    private LocalDateTime startTime;
}