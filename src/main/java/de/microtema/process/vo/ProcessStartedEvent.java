package de.microtema.process.vo;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class ProcessStartedEvent extends ProcessStartEvent {

    @NotEmpty
    private String processInstanceId;
}