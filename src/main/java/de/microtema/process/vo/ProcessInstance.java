package de.microtema.process.vo;

import de.microtema.common.enums.ProcessStatus;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
public class ProcessInstance {

    @NotNull
    private ProcessStatus instanceStatus;

    @NotNull
    private String instanceDescription;

    private String instanceId;

    @NotNull
    private String processBusinessKey;

    @NotNull
    private String instanceName;

    @NotNull
    private String definitionKey;

    @NotEmpty
    private String starterId;

    @NotEmpty
    private String referenceId;

    @NotEmpty
    private String referenceType;

    @NotNull
    private String referenceValue;

    @NotNull
    private LocalDateTime instanceStartTime;

    private LocalDateTime instanceEndTime;

    private long duration;
}
