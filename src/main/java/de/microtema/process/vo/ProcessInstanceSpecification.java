package de.microtema.process.vo;

import lombok.Builder;
import lombok.Value;

import javax.validation.constraints.NotNull;

@Builder
@Value
public class ProcessInstanceSpecification {

    @NotNull
    String query;
}