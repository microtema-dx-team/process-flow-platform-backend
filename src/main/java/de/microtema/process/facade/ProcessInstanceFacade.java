package de.microtema.process.facade;

import de.microtema.process.service.ProcessInstanceReadService;
import de.microtema.process.vo.ProcessInstance;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ProcessInstanceFacade {

    private final ProcessInstanceReadService service;

    public Page<ProcessInstance> getProcesses(int page, int size, String properties, String query) {

        return service.getProcesses(page, size, properties, query);
    }

    public ProcessInstance getProcessByProcessBusinessKey(String processBusinessKey) {

        return service.getProcessByProcessBusinessKey(processBusinessKey);
    }
}
