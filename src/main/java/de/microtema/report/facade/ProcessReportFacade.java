package de.microtema.report.facade;

import de.microtema.report.service.ProcessReportReadService;
import de.microtema.report.vo.ProcessReport;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class ProcessReportFacade {

    private final ProcessReportReadService service;

    public List<ProcessReport> getProcessReports(String processBusinessKey) {

        return service.getProcessReports(processBusinessKey);
    }

    public ProcessReport getProcessReport(UUID uuid) {

        return service.getProcessReport(uuid);
    }
}
