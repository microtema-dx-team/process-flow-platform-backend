package de.microtema.report.repository;

import de.microtema.common.repository.BaseEntityRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProcessReportRepository extends BaseEntityRepository<ProcessReportEntity> {

    @Override
    default Optional<ProcessReportEntity> findOne(ProcessReportEntity entity) {

        String processBusinessKey = entity.getProcessBusinessKey();
        String reportId = entity.getReportId();

        return findByProcessBusinessKeyAndReportId(processBusinessKey, reportId);
    }

    Optional<ProcessReportEntity> findByProcessBusinessKeyAndReportId(String processBusinessKey, String reportId);

    List<ProcessReportEntity> findAllByProcessBusinessKeyOrderByReportStartTimeAsc(String processBusinessKey);
}
