package de.microtema.report.repository;

import de.microtema.common.enums.ProcessStatus;
import de.microtema.common.repository.BaseEntity;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@Entity(name = "ProcessInstanceReport")
@Table(name = "ProcessInstanceReport")
public class ProcessReportEntity extends BaseEntity implements Comparable<ProcessReportEntity> {

    @NotNull
    private String processBusinessKey;

    @NotNull
    private String reportId;

    @NotNull
    private String reportName;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private ProcessStatus reportStatus;

    private String reportDescription;

    @NotNull
    private LocalDateTime reportStartTime;

    private LocalDateTime reportEndTime;

    @Override
    public int compareTo(ProcessReportEntity o) {

        return o.getReportStartTime().compareTo(reportStartTime);
    }
}