package de.microtema.report.kafka;

import de.microtema.common.converter.EventToBeanConverter;
import de.microtema.report.service.ProcessReportService;
import de.microtema.report.vo.ProcessReportEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

@Log4j2
@Component
@RequiredArgsConstructor
public class ProcessReportListener {

    private final ProcessReportService processReportService;

    private final ProcessReportProperties processReportProperties;

    private final EventToBeanConverter<ProcessReportEvent> eventConverter;

    @SendTo("#{processReportProperties.errorTopicName}")
    @KafkaListener(topics = "#{processReportProperties.topicName}", errorHandler = "kafkaListenerErrorHandler")
    public void listen(@Payload String event) {

        log.info(() -> "Receive event: " + processReportProperties.getTopicName());

        ProcessReportEvent processReportEvent = eventConverter.convert(event, ProcessReportEvent.class);

        processReportService.saveProcessReport(processReportEvent);
    }
}
