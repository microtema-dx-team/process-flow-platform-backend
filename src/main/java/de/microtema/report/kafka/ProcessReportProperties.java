package de.microtema.report.kafka;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "process-reports")
public class ProcessReportProperties {

    private String topicName;

    private String errorTopicName;
}
