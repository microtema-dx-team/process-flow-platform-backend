package de.microtema.report.update;

import de.microtema.common.enums.ProcessStatus;
import de.microtema.model.converter.Updater;
import de.microtema.report.repository.ProcessReportEntity;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class ProcessReportEntityUpdater implements Updater<ProcessReportEntity, ProcessReportEntity> {

    @Override
    public void update(ProcessReportEntity dest, ProcessReportEntity orig) {

        ProcessStatus reportStatus = dest.getReportStatus();

        if (Objects.nonNull(reportStatus) && !reportStatus.before(orig.getReportStatus())) {
            return;
        }

        dest.setReportStatus(orig.getReportStatus());
        dest.setReportEndTime(orig.getReportEndTime());
    }
}
