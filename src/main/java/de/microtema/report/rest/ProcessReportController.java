package de.microtema.report.rest;

import de.microtema.report.facade.ProcessReportFacade;
import de.microtema.report.vo.ProcessReport;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/rest/report")
public class ProcessReportController {

    private final ProcessReportFacade facade;

    @GetMapping
    public ResponseEntity<List<ProcessReport>> getProcessReports(@RequestParam(name = "processBusinessKey", required = false) String processBusinessKey) {

        return ResponseEntity.ok(facade.getProcessReports(processBusinessKey));
    }

    @GetMapping(value = "/{uuid}")
    public ResponseEntity<ProcessReport> getProcessReport(@PathVariable UUID uuid) {

        return ResponseEntity.ok(facade.getProcessReport(uuid));
    }
}
