package de.microtema.report.converter;

import de.microtema.model.converter.Converter;
import de.microtema.report.repository.ProcessReportEntity;
import de.microtema.report.vo.ProcessReport;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

@Component
public class ProcessReportEntityToProcessReportConverter implements Converter<ProcessReport, ProcessReportEntity> {

    @Override
    public void update(ProcessReport dest, ProcessReportEntity orig) {

        dest.setId(orig.getUuid());

        dest.setProcessStatus(orig.getReportStatus());
        dest.setDescription(orig.getReportDescription());

        dest.setActivityId(orig.getReportId());
        dest.setActivityName(orig.getReportName());

        dest.setReportStartTime(orig.getReportStartTime());
        dest.setReportEndTime(orig.getReportEndTime());
        dest.setDuration(getDuration(dest.getReportStartTime(), dest.getReportEndTime()));
    }

    private long getDuration(LocalDateTime instanceStartTime, LocalDateTime instanceEndTime) {

        return ChronoUnit.MILLIS.between(instanceStartTime, Optional.ofNullable(instanceEndTime).orElse(LocalDateTime.now()));
    }
}
