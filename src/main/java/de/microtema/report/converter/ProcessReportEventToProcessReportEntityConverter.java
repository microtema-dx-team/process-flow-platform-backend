package de.microtema.report.converter;

import de.microtema.common.enums.ProcessStatus;
import de.microtema.model.converter.Converter;
import de.microtema.report.repository.ProcessReportEntity;
import de.microtema.report.vo.ProcessReportEvent;
import org.springframework.stereotype.Component;

@Component
public class ProcessReportEventToProcessReportEntityConverter implements Converter<ProcessReportEntity, ProcessReportEvent> {

    @Override
    public void update(ProcessReportEntity dest, ProcessReportEvent orig) {

        ProcessStatus processStatus = orig.getProcessStatus();

        dest.setProcessBusinessKey(orig.getProcessBusinessKey());
        dest.setReportStatus(processStatus);
        dest.setReportId(orig.getActivityId());
        dest.setReportName(orig.getActivityName());
        dest.setReportDescription(orig.getDescription());

        switch (processStatus) {
            case STARTED:
                dest.setReportStartTime(orig.getEventTime());
                break;
            case SUSPENDED:
            case ABORTED:
            case WARNING:
            case ERROR:
            case ENDED:
            case COMPLETED:
                dest.setReportEndTime(orig.getEventTime());
                break;
        }
    }
}
