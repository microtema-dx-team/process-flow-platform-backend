package de.microtema.report.vo;

import de.microtema.common.enums.ProcessStatus;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
public class ProcessReportEvent {

    @NotNull
    private String activityId;

    @NotNull
    private String activityName;

    @NotNull
    private ProcessStatus processStatus;

    @NotNull
    private String processBusinessKey;

    private String description;

    @NotNull
    private LocalDateTime eventTime;
}