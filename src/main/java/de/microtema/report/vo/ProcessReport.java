package de.microtema.report.vo;

import de.microtema.common.enums.ProcessStatus;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
public class ProcessReport {

    private UUID id;

    @NotNull
    private String activityId;

    @NotNull
    private String activityName;

    @NotNull
    private ProcessStatus processStatus;

    @NotNull
    private String processBusinessKey;

    private String description;

    @NotNull
    private LocalDateTime reportStartTime;

    private LocalDateTime reportEndTime;

    private long duration;
}