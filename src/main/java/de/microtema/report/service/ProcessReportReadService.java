package de.microtema.report.service;

import de.microtema.report.converter.ProcessReportEntityToProcessReportConverter;
import de.microtema.report.repository.ProcessReportEntity;
import de.microtema.report.repository.ProcessReportRepository;
import de.microtema.report.vo.ProcessReport;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ProcessReportReadService {

    private final ProcessReportRepository repository;

    private final ProcessReportEntityToProcessReportConverter reportConverter;

    public List<ProcessReport> getProcessReports(String processBusinessKey) {

        List<ProcessReportEntity> reports = repository.findAllByProcessBusinessKeyOrderByReportStartTimeAsc(processBusinessKey);

        return reportConverter.convertList(reports);
    }

    public ProcessReport getProcessReport(UUID uuid) {

        ProcessReportEntity reportEntity = repository.getOne(uuid);

        return reportConverter.convert(reportEntity);
    }
}
