package de.microtema.report.service;

import de.microtema.process.service.ProcessInstanceWriteService;
import de.microtema.report.converter.ProcessReportEventToProcessReportEntityConverter;
import de.microtema.report.repository.ProcessReportEntity;
import de.microtema.report.repository.ProcessReportRepository;
import de.microtema.report.update.ProcessReportEntityUpdater;
import de.microtema.report.vo.ProcessReportEvent;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProcessReportService {

    private final ProcessReportRepository repository;

    private final ProcessReportEntityUpdater entityUpdater;

    private final ProcessInstanceWriteService processInstanceWriteService;

    private final ProcessReportEventToProcessReportEntityConverter entityConverter;

    public synchronized void saveProcessReport(ProcessReportEvent processReportEvent) {

        ProcessReportEntity processReportEntity = entityConverter.convert(processReportEvent);

        Optional<ProcessReportEntity> recent = repository.findOne(processReportEntity);

        if (recent.isPresent()) {

            updateProcessReport(recent.get(), processReportEntity);

        } else {

            repository.saveAndFlush(processReportEntity);
        }

        processInstanceWriteService.updateProcessInstance(processReportEvent);
    }

    private void updateProcessReport(ProcessReportEntity recent, ProcessReportEntity processReportEntity) {

        entityUpdater.update(recent, processReportEntity);

        repository.saveAndFlush(recent);
    }
}
