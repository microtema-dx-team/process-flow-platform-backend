package de.microtema.definition.repository;

import de.microtema.common.repository.BaseEntityRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProcessDefinitionRepository extends BaseEntityRepository<ProcessDefinitionEntity> {

    @Override
    default Optional<ProcessDefinitionEntity> findOne(ProcessDefinitionEntity entity) {

        String definitionKey = entity.getDefinitionKey();
        Integer definitionMajorVersion = entity.getDefinitionMajorVersion();

        return findByDefinitionKeyAndDefinitionMajorVersionOrderByDefinitionVersionDesc(definitionKey, definitionMajorVersion);
    }

    Optional<ProcessDefinitionEntity> findByDefinitionKeyAndDefinitionMajorVersionOrderByDefinitionVersionDesc(String definitionKey, int majorVersion);

    Optional<ProcessDefinitionEntity> findByDefinitionKey(String definitionKey);
}
