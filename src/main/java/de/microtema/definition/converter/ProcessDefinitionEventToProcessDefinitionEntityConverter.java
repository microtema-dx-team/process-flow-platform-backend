package de.microtema.definition.converter;

import de.microtema.definition.repository.ProcessDefinitionEntity;
import de.microtema.definition.vo.ProcessDefinitionEvent;
import de.microtema.model.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ProcessDefinitionEventToProcessDefinitionEntityConverter implements Converter<ProcessDefinitionEntity, ProcessDefinitionEvent> {

    @Override
    public void update(ProcessDefinitionEntity dest, ProcessDefinitionEvent orig) {

        dest.setDefinitionName(orig.getProcessId());
        dest.setDefinitionDisplayName(orig.getDisplayName());
        dest.setDefinitionDescription(orig.getDescription());

        dest.setDefinitionDiagram(orig.getDiagram());
        dest.setDefinitionDeployTime(orig.getDeployTime());

        dest.setDefinitionVersion(orig.getDefinitionVersion());
        dest.setDefinitionMajorVersion(orig.getMajorVersion());
        dest.setDefinitionKey(orig.getDefinitionKey());
        dest.setBoundedContext(orig.getBoundedContext());
    }
}
