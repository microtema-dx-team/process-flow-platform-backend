package de.microtema.definition.converter;

import de.microtema.definition.repository.ProcessDefinitionEntity;
import de.microtema.definition.vo.ProcessDefinition;
import de.microtema.model.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ProcessDefinitionEntityToProcessDefinitionConverter implements Converter<ProcessDefinition, ProcessDefinitionEntity> {

    @Override
    public void update(ProcessDefinition dest, ProcessDefinitionEntity orig) {

        dest.setDefinitionKey(orig.getDefinitionKey());
        dest.setBoundedContext(orig.getBoundedContext());
        dest.setName(orig.getDefinitionName());
        dest.setDisplayName(orig.getDefinitionDisplayName());
        dest.setDescription(orig.getDefinitionDescription());

        dest.setDeployTime(orig.getDefinitionDeployTime());
        dest.setMajorVersion(orig.getDefinitionMajorVersion());
        dest.setDefinitionVersion(orig.getDefinitionVersion());

        dest.setDiagram(orig.getDefinitionDiagram());
        dest.setId(orig.getUuid());
    }
}
