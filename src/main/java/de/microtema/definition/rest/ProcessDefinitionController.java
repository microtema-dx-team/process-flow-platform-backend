package de.microtema.definition.rest;

import de.microtema.definition.facade.ProcessDefinitionFacade;
import de.microtema.definition.vo.ProcessDefinition;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/rest/definition")
public class ProcessDefinitionController {

    private final ProcessDefinitionFacade facade;

    @GetMapping
    public ResponseEntity<Page<ProcessDefinition>> getProcessDefinitions(
            @RequestParam(name = "page", required = false, defaultValue = "0") int page,
            @RequestParam(name = "size", required = false, defaultValue = "25") int size,
            @RequestParam(name = "properties", required = false) String properties,
            @RequestParam(name = "query", required = false) String query) {

        return ResponseEntity.ok(facade.getProcessDefinitions(page, size, properties, query));
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<ProcessDefinition> getProcessDefinition(@PathVariable UUID id) {

        return ResponseEntity.ok(facade.getProcessDefinition(id));
    }

    @GetMapping(value = "/key/{definitionKey}")
    public ResponseEntity<ProcessDefinition> getProcessDefinitionByDefinitionKey(@PathVariable String definitionKey) {

        return ResponseEntity.ok(facade.getProcessDefinitionByDefinitionKey(definitionKey));
    }
}
