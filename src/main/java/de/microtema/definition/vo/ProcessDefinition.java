package de.microtema.definition.vo;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
public class ProcessDefinition {

    @NotNull
    private UUID id;

    @NotNull
    private String definitionKey;

    @NotNull
    private String boundedContext;

    @NotNull
    private String name;

    @NotNull
    private String displayName;

    @NotNull
    private Integer definitionVersion;

    @NotNull
    private Integer majorVersion;

    private String description;

    private String diagram;

    @NotNull
    private LocalDateTime deployTime;
}
