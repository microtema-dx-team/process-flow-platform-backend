package de.microtema.definition.vo;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
public class ProcessDefinitionEvent {

    @NotNull
    private String definitionKey;

    @NotNull
    private Integer definitionVersion;

    @NotNull
    private Integer majorVersion;

    @NotNull
    private String boundedContext;

    @NotNull
    private String processId;

    @NotNull
    private String displayName;

    @NotNull
    private String processName;


    private String description;

    @NotNull
    private String diagram;

    @NotNull
    private LocalDateTime deployTime;
}
