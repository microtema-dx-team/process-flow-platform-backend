package de.microtema.definition.facade;

import de.microtema.definition.service.ProcessDefinitionService;
import de.microtema.definition.vo.ProcessDefinition;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.UUID;


@Component
@RequiredArgsConstructor
public class ProcessDefinitionFacade {

    private final ProcessDefinitionService service;

    public Page<ProcessDefinition> getProcessDefinitions(int page, int size, String properties, String query) {

        return service.getProcessDefinitions(page, size, properties, query);
    }

    public ProcessDefinition getProcessDefinition(UUID id) {

        return service.getProcessDefinition(id);
    }

    public ProcessDefinition getProcessDefinitionByDefinitionKey(String definitionKey) {

        return service.getProcessDefinitionByDefinitionKey(definitionKey);
    }
}
