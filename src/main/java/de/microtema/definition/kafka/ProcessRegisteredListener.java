package de.microtema.definition.kafka;

import de.microtema.common.converter.EventToBeanConverter;
import de.microtema.definition.service.ProcessDefinitionService;
import de.microtema.definition.vo.ProcessDefinitionEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

@Log4j2
@Component
@RequiredArgsConstructor
public class ProcessRegisteredListener {

    private final ProcessDefinitionService service;
    private final RegisterProcessProperties registerProcessProperties;
    private final EventToBeanConverter<ProcessDefinitionEvent> eventConverter;

    @SendTo("#{registerProcessProperties.errorTopicName}")
    @KafkaListener(topics = "#{registerProcessProperties.topicName}", errorHandler = "kafkaListenerErrorHandler")
    public void listen(@Payload String event) {

        log.info(() -> "Receive event: " + registerProcessProperties.getTopicName());

        ProcessDefinitionEvent processDefinition = eventConverter.convert(event, ProcessDefinitionEvent.class);

        service.saveProcessDefinition(processDefinition);
    }
}
