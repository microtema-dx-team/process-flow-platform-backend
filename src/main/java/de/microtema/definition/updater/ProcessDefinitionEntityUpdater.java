package de.microtema.definition.updater;

import de.microtema.definition.repository.ProcessDefinitionEntity;
import de.microtema.model.converter.Updater;
import org.springframework.stereotype.Component;

@Component
public class ProcessDefinitionEntityUpdater implements Updater<ProcessDefinitionEntity, ProcessDefinitionEntity> {

    @Override
    public void update(ProcessDefinitionEntity dest, ProcessDefinitionEntity orig) {

        dest.setDefinitionDeployTime(orig.getDefinitionDeployTime());
    }
}
