package de.microtema.definition.service;

import de.microtema.common.converter.PageRequestDTOToPageRequestConverter;
import de.microtema.definition.converter.ProcessDefinitionEntityToProcessDefinitionConverter;
import de.microtema.definition.converter.ProcessDefinitionEventToProcessDefinitionEntityConverter;
import de.microtema.definition.converter.ProcessDefinitionSpecificationToSpecificationConverter;
import de.microtema.definition.repository.ProcessDefinitionEntity;
import de.microtema.definition.repository.ProcessDefinitionRepository;
import de.microtema.definition.updater.ProcessDefinitionEntityUpdater;
import de.microtema.definition.vo.PageRequestDTO;
import de.microtema.definition.vo.ProcessDefinition;
import de.microtema.definition.vo.ProcessDefinitionEvent;
import de.microtema.definition.vo.ProcessDefinitionSpecification;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

@Service
@Transactional
@RequiredArgsConstructor
public class ProcessDefinitionService {

    private final ProcessDefinitionRepository repository;
    private final ProcessDefinitionEntityUpdater entityUpdater;
    private final PageRequestDTOToPageRequestConverter pageRequestConverter;
    private final ProcessDefinitionEntityToProcessDefinitionConverter converter;
    private final ProcessDefinitionEventToProcessDefinitionEntityConverter entityConverter;
    private final ProcessDefinitionSpecificationToSpecificationConverter specificationConverter;

    public Page<ProcessDefinition> getProcessDefinitions(int page, int size, String properties, String query) {

        ProcessDefinitionSpecification processDefinitionSpecification = ProcessDefinitionSpecification.builder().query(query).groupBy("definitionKey").build();
        PageRequestDTO pageRequestDTO = PageRequestDTO.of(page, size, properties);

        Specification<ProcessDefinitionEntity> specification = specificationConverter.convert(processDefinitionSpecification);
        Pageable pageable = pageRequestConverter.convert(pageRequestDTO);

        return repository.findAll(specification, pageable).map(converter::convert);
    }

    public void saveProcessDefinition(ProcessDefinitionEvent processDefinition) {

        ProcessDefinitionEntity processDefinitionEntity = entityConverter.convert(processDefinition);
        Optional<ProcessDefinitionEntity> recent = repository.findOne(processDefinitionEntity);

        if (recent.isPresent()) {

            updateProcessDefinition(recent.get(), processDefinitionEntity);

        } else {

            repository.save(processDefinitionEntity);
        }
    }

    public ProcessDefinition getProcessDefinition(UUID id) {

        ProcessDefinitionEntity entity = repository.getOne(id);

        return converter.convert(entity);
    }

    public ProcessDefinition getProcessDefinitionByDefinitionKey(String definitionKey) {

        return repository.findByDefinitionKey(definitionKey).map(converter::convert).orElse(null);
    }

    private void updateProcessDefinition(ProcessDefinitionEntity recent, ProcessDefinitionEntity processDefinitionEntity) {

        entityUpdater.update(recent, processDefinitionEntity);

        repository.save(recent);
    }
}
